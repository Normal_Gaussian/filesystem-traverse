import { traverse, exampleDir } from ".";

// This directory is structured such that there are six example
// files "exFileA", "exFileB", "exFileC", "aFileEx", "bFileEx",
// "cFileEx" and two directories "exDirA", "exDirB" each containing
// a further six example files and a further two directories each
// containing a further six example files. In short, there are 42
// files in 7 different directories, at 3 different depths.

it("should list all files in directories not ending with A", async () => {
  expect.assertions(4);
  const expectMatchingFiles = 18;

  // We mock the process call to pass through the filename, and spyon the
  // mock to check it was called for every file.
  const mockFn = jest.fn(filepath => filepath);

  const result = await traverse({
    directory: exampleDir,
    process_file: mockFn,
    exclude_dir: "A$"
  });

  expect(mockFn).toHaveBeenCalledTimes(expectMatchingFiles);
  expect(result).toHaveLength(expectMatchingFiles);

  // Check that all items are unique
  expect(
    result.filter((filename, index, array) => array.indexOf(filename) === index)
  ).toHaveLength(expectMatchingFiles);

  expect(result).toContainSameElementsAs([
      "aFileEx",
      "bFileEx",
      "cFileEx",
      "exFileA",
      "exFileB",
      "exFileC",
      "exDirB/aFileEx",
      "exDirB/bFileEx",
      "exDirB/cFileEx",
      "exDirB/exFileA",
      "exDirB/exFileB",
      "exDirB/exFileC",
      "exDirB/exDirB/aFileEx",
      "exDirB/exDirB/bFileEx",
      "exDirB/exDirB/cFileEx",
      "exDirB/exDirB/exFileA",
      "exDirB/exDirB/exFileB",
      "exDirB/exDirB/exFileC",
    ]);
});
