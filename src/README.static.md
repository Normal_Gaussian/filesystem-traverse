## traverse<T>(options<T>): Promise<Array<T>>

Traverses the filesystem applying `process_file` to all files under the given `directory`. 

### arguments

options:

```ts
{
  directory?: string,
  process_file: (filepath: string) => T,
  include_file?: string | RegExp,
  exclude_file?: string | RegExp,
  include_dir?: string | RegExp,
  exclude_dir?: string | RegExp,
  fs?: typeof fs
}
```

* `directory?: string` - The directory from which to recursively process files. Defaults to the current working directory of the process.
* `process_file: (filepath: string) => T` - A function to be called on each file traversed; the return values become members of the result array.
* `include_file: string | RegExp` - An optional regex filter that is applied to discovered files. Only files that match are processed - unless they match the `exclude_file` filter. Defaults to all files.
* `exclude_file: string | RegExp` - An optional regex filter that is applied to discovered files. Only files that do not match are processed. Defaults to no files.
* `include_dir: string | RegExp` - An optional regex filter that is applied to discovered directories. If a directory does not match it will not be entered and the files inside will not be processed. Defaults to all directories.
* `exclude_dir: string | RegExp` - An optional regex filter that is applied to discovered directories. If a directory matches it will not be entered and the files inside will not be processed. Defaults to no directories.
* `fs: FS` - An optional fs module, such as memfs. Defaults to the NodeJS fs module.

### return

* `Promise<Array<T>>` - A promise that resolves once all of the `process_file` functions return, and resolves to an array of the return values of `process_file`. The order of the array is not guaranteed.

### Examples

