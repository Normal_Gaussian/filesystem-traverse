import { traverse, exampleDir } from ".";

// This directory is structured such that there are six example
// files "exFileA", "exFileB", "exFileC", "aFileEx", "bFileEx",
// "cFileEx" and two directories "exDirA", "exDirB" each containing
// a further six example files and a further two directories each
// containing a further six example files. In short, there are 42
// files in 7 different directories, at 3 different depths.

it("should list all files", async () => {
  expect.assertions(4);
  const expectedMatchingFiles = 42;

  // We mock the process call to pass through the filename, and spyon the
  // mock to check it was called for every file.
  const mockFn = jest.fn(filepath => filepath);

  const result = await traverse({
    directory: exampleDir,
    process_file: mockFn
  });

  expect(mockFn).toHaveBeenCalledTimes(expectedMatchingFiles);
  expect(result).toHaveLength(expectedMatchingFiles);

  // Check that all items are unique
  expect(
    result.filter((filename, index, array) => array.indexOf(filename) === index)
  ).toHaveLength(expectedMatchingFiles);

  expect(result).toContainSameElementsAs([
      "aFileEx",
      "bFileEx",
      "cFileEx",
      "exFileA",
      "exFileB",
      "exFileC",
      "exDirA/aFileEx",
      "exDirA/bFileEx",
      "exDirA/cFileEx",
      "exDirA/exFileA",
      "exDirA/exFileB",
      "exDirA/exFileC",
      "exDirA/exDirA/aFileEx",
      "exDirA/exDirA/bFileEx",
      "exDirA/exDirA/cFileEx",
      "exDirA/exDirA/exFileA",
      "exDirA/exDirA/exFileB",
      "exDirA/exDirA/exFileC",
      "exDirA/exDirB/aFileEx",
      "exDirA/exDirB/bFileEx",
      "exDirA/exDirB/cFileEx",
      "exDirA/exDirB/exFileA",
      "exDirA/exDirB/exFileB",
      "exDirA/exDirB/exFileC",
      "exDirB/aFileEx",
      "exDirB/bFileEx",
      "exDirB/cFileEx",
      "exDirB/exFileA",
      "exDirB/exFileB",
      "exDirB/exFileC",
      "exDirB/exDirA/aFileEx",
      "exDirB/exDirA/bFileEx",
      "exDirB/exDirA/cFileEx",
      "exDirB/exDirA/exFileA",
      "exDirB/exDirA/exFileB",
      "exDirB/exDirA/exFileC",
      "exDirB/exDirB/aFileEx",
      "exDirB/exDirB/bFileEx",
      "exDirB/exDirB/cFileEx",
      "exDirB/exDirB/exFileA",
      "exDirB/exDirB/exFileB",
      "exDirB/exDirB/exFileC",
    ]);
});
