const traversePath = process.env.NODE_ENV === "production" ? '../dist/index.js' : '../src/index.static.ts';

expect.extend({
  toContainSameElementsAs(received, expecting) {
    if(!Array.isArray(expecting)) {
      throw new Error(`Expected must be an Array`);
    }
    if(!Array.isArray(received)) {
      throw new Error(`Received should be an Array`);
    }

    if(received.length !== expecting.length) {
      return {
        message: () => `length mismatch`,
        pass: false
      }
    }

    if(!expecting.every(exp => received.some(rec => Object.is(exp, rec)))) {
      return {
        message: () => `missing elements`,
        pass: false
      }
    }
    
    if(!received.every(rec => expecting.some(exp => Object.is(exp, rec)))) {
      return {
        message: () => `additional elements`,
        pass: false
      }
    }

    return {
      message: () => `contains same elements`,
      pass: true
    }
  }
})

export const traverse = require(traversePath).default;
export const exampleDir = __dirname + '/exampleDir';