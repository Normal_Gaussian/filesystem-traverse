import { traverse, exampleDir } from ".";

// This directory is structured such that there are six example
// files "exFileA", "exFileB", "exFileC", "aFileEx", "bFileEx",
// "cFileEx" and two directories "exDirA", "exDirB" each containing
// a further six example files and a further two directories each
// containing a further six example files. In short, there are 42
// files in 7 different directories, at 3 different depths.

it("should list all files starting with ex - string", async () => {
  expect.assertions(4);
  const expectMatchingFiles = 21;

  // We mock the process call to pass through the filename, and spyon the
  // mock to check it was called for every file.
  const mockFn = jest.fn(filepath => filepath);

  const result = await traverse({
    directory: exampleDir,
    process_file: mockFn,
    include_file: "^ex"
  });

  expect(mockFn).toHaveBeenCalledTimes(expectMatchingFiles);
  expect(result).toHaveLength(expectMatchingFiles);

  // Check that all items are unique
  expect(
    result.filter((filename, index, array) => array.indexOf(filename) === index)
  ).toHaveLength(expectMatchingFiles);

  expect(result).toContainSameElementsAs([
      "exFileA",
      "exFileB",
      "exFileC",
      "exDirA/exFileA",
      "exDirA/exFileB",
      "exDirA/exFileC",
      "exDirA/exDirA/exFileA",
      "exDirA/exDirA/exFileB",
      "exDirA/exDirA/exFileC",
      "exDirA/exDirB/exFileA",
      "exDirA/exDirB/exFileB",
      "exDirA/exDirB/exFileC",
      "exDirB/exFileA",
      "exDirB/exFileB",
      "exDirB/exFileC",
      "exDirB/exDirA/exFileA",
      "exDirB/exDirA/exFileB",
      "exDirB/exDirA/exFileC",
      "exDirB/exDirB/exFileA",
      "exDirB/exDirB/exFileB",
      "exDirB/exDirB/exFileC",
    ]);
});

it("should list all files starting with ex - regex", async () => {
  expect.assertions(4);
  const expectMatchingFiles = 21;

  // We mock the process call to pass through the filename, and spyon the
  // mock to check it was called for every file.
  const mockFn = jest.fn(filepath => filepath);

  const result = await traverse({
    directory: exampleDir,
    process_file: mockFn,
    include_file: /^ex/
  });

  expect(mockFn).toHaveBeenCalledTimes(expectMatchingFiles);
  expect(result).toHaveLength(expectMatchingFiles);

  // Check that all items are unique
  expect(
    result.filter((filename, index, array) => array.indexOf(filename) === index)
  ).toHaveLength(expectMatchingFiles);

  expect(result).toContainSameElementsAs([
      "exFileA",
      "exFileB",
      "exFileC",
      "exDirA/exFileA",
      "exDirA/exFileB",
      "exDirA/exFileC",
      "exDirA/exDirA/exFileA",
      "exDirA/exDirA/exFileB",
      "exDirA/exDirA/exFileC",
      "exDirA/exDirB/exFileA",
      "exDirA/exDirB/exFileB",
      "exDirA/exDirB/exFileC",
      "exDirB/exFileA",
      "exDirB/exFileB",
      "exDirB/exFileC",
      "exDirB/exDirA/exFileA",
      "exDirB/exDirA/exFileB",
      "exDirB/exDirA/exFileC",
      "exDirB/exDirB/exFileA",
      "exDirB/exDirB/exFileB",
      "exDirB/exDirB/exFileC",
    ]);
});

it("should list all files ending with A", async () => {
  expect.assertions(4);
  const expectMatchingFiles = 7;

  // We mock the process call to pass through the filename, and spyon the
  // mock to check it was called for every file.
  const mockFn = jest.fn(filepath => filepath);

  const result = await traverse({
    directory: exampleDir,
    process_file: mockFn,
    include_file: /A$/
  });

  expect(mockFn).toHaveBeenCalledTimes(expectMatchingFiles);
  expect(result).toHaveLength(expectMatchingFiles);

  // Check that all items are unique
  expect(
    result.filter((filename, index, array) => array.indexOf(filename) === index)
  ).toHaveLength(expectMatchingFiles);

  expect(result).toContainSameElementsAs([
      "exFileA",
      "exDirA/exFileA",
      "exDirA/exDirA/exFileA",
      "exDirA/exDirB/exFileA",
      "exDirB/exFileA",
      "exDirB/exDirA/exFileA",
      "exDirB/exDirB/exFileA",
    ]);
});
