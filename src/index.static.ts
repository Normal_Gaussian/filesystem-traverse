import path from 'path';
import systemFS from 'fs';
type FS = typeof systemFS;

type Context = {
  fs: FS,
  basedir: string,
  includeFile: (file: string) => boolean,
  includeDirectory: (directory: string) => boolean,
};

type Options = {
  directory?: string,
  include_file?: string | RegExp,
  exclude_file?: string | RegExp,
  include_dir?: string | RegExp,
  exclude_dir?: string | RegExp,
  fs?: FS
};

function makeAsync<T, A, F extends (args: A) => T | Promise<T>>(func: F): (args: A) => Promise<T> {
  return async (arg: A) => func(arg);
}

/**
 * Creates an inclusion filter based on include and exclude options
 *  that can be either strings or regular expressions.
 */
function makeInclude(include: string | RegExp, exclude: string | RegExp): (string: string) => boolean {
  const includeFilter = include && new RegExp(include);
  const excludeFilter = exclude && new RegExp(exclude);
  return function(filepath) {
    // If an include filter is set and it is not included, do not include
    if(includeFilter && !includeFilter.test(filepath)) {
      return false;
    }
    // If an exclude filter is set and it is excluded, do not include
    if(excludeFilter && excludeFilter.test(filepath)) {
      return false;
    }
    // default to include
    return true;
  }
}

/**
 * Search the filesystem until a file is found that matches the match condition
 */
async function search(match: (file: string) => Promise<boolean>, context: Context, directory: string, state: {complete: boolean} = { complete: false}): Promise<void> {
  const { fs, basedir, includeFile, includeDirectory } = context;
  if(state.complete) {
    return;
  }

  /* Get a list of everything in the current directory
   *  that is not excluded by a filter
   */
  const entries = await fs.promises.readdir(
    path.join(basedir, directory),
    {
      encoding: 'utf8',
      withFileTypes: true 
    }
  );
  
  const files = entries
    .filter(entry => entry.isFile() && includeFile(entry.name))
    .map(entry => path.join(directory, entry.name));

  const directories = entries
    .filter(entry => entry.isDirectory() && includeDirectory(entry.name))
    .map(entry => path.join(directory, entry.name));

  /* Start the checks to see if each file is a match,
   *  start the search in each directory
   */
  const fileSearch = files.map(async (file) => {
    if(!state.complete) {
      const result = await match(file);
      if(result) {
        state.complete = true;
      }
    }
  });

  const directorySearch = directories.map(directory => search(match, context, directory, state));
  
  // Await all subsearches to complete
  await Promise.all([...fileSearch, ...directorySearch]);
}

function optionsToContext(options: Options): Context {
  const basedir = path.isAbsolute(options.directory || "") ? options.directory || "" : path.resolve(path.join(process.cwd(), options.directory || ""));

  const includeFile = makeInclude(options.include_file, options.exclude_file);
  const includeDirectory = makeInclude(options.include_dir, options.exclude_dir);

  const fs = options.fs || systemFS;

  return {
    fs,
    basedir,
    includeFile,
    includeDirectory,
  }
}

/* traverse
 * 
 * Traverse a filesystem processing each file encountered.
 * Starting at the current working directory or a provided directory.
 * Accepts regex filters.
 * Returns a Promise providing an array of function results.
 * Allows passing an fs object (for memfs etc.)
 */
async function traverse<T>(options: Options & {
  process_file: (filepath: string) => T,
}): Promise<Array<T>> {
  if(typeof options.process_file !== 'function') {
    throw new Error("process_file must be a function");
  }
  const processFile = options.process_file;

  const context = optionsToContext(options);

  const results = [];
  await search(async (file) => {
      results.push(processFile(file));
      return false;
    },
    context, "./");
  return results;
}

// * some
export async function some(match: (file: string) => boolean | Promise<boolean>, options: Options): Promise<boolean> {
  if(typeof match != 'function') {
    throw new Error("match must be a function");
  }
  
  const asyncMatch = makeAsync<boolean, string, typeof match>(match);
  
  const context = optionsToContext(options);
  
  const state = { complete: false };
  await search(asyncMatch, context, "./", state);
  return state.complete;
}

//  * ever/y
export async function every(match: (file: string) => boolean | Promise<boolean>, options: Options): Promise<boolean> {
  if(typeof match !== 'function') {
    throw new Error("match must be a function");
  }

  const asyncMatch = makeAsync(match);
  
  const context = optionsToContext(options);
  
  let every = true;
  await search(
    async (file: string) => {

      // If another matcher failed, then exit the some
      if(!every) {
        return true; // The return value doesn't matter
      }
      
      // Test for a match
      const isMatch = await asyncMatch(file);
      if(!isMatch) {
        every = false;
        return true; // Cause the some to stop
      }
      
      return false; // Cause the some to continue to exhaustion
    },
    context,
    "./"
  );
  return every;
}

//  * filter
export async function filter(match: (file: string) => boolean | Promise<boolean>, options: Options): Promise<Array<string>> {
  if(typeof match !== 'function') {
    throw new Error("match must be a function");
  }

  const asyncMatch = makeAsync(match);
  
  const context = optionsToContext(options);
  
  const results: Array<string> = [];
  await search(
    async (file: string) => {
      // Test for a match
      const isMatch = await asyncMatch(file);
      if(isMatch) {
        results.push(file);
      }
      
      return false; // Cause the some to continue to exhaustion
    },
    context,
    "./"
  );
  return results;
}

//  * find
//  * flat    ?
//  * flatMap ?
//  * forEach
export async function forEach(apply: (file: string) => void | Promise<void>, options: Options): Promise<void> {
  if(typeof apply !== 'function') {
    throw new Error("match must be a function");
  }

  const asyncApply = makeAsync(apply);
  const context = optionsToContext(options);
  
  await search(
    async (file: string) => {
      await asyncApply(file);
      return false; // Cause the some to continue to exhaustion
    },
    context,
    "./"
  );
}
//  * includes
//  * keys
//  * map

//  * reduce

export default traverse;
  